package view;

import java.text.ParseException;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.MyLinkedList;
import model.data_structures.MyStack;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.InfoTaxiRango2;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;
import model.vo.infoCompaņiaRango;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{
	

	public static void main(String[] args) throws ParseException 
	{
		
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
			

				case 3:
					for(int i=0; i<TaxiTripsManager.DIRECCION_LARGE_JSON.length; i++)
					{
						linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON[i];
						Controller.cargarSistema(linkJson);
					}
					break;
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1A	
			case 2:

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq1A = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq1A = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq1A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq1A = sc.next();
				
				
				//VORangoFechaHora
				RangoFechaHora rangoReq1A = new RangoFechaHora(fechaInicialReq1A, fechaFinalReq1A, horaInicialReq1A, horaFinalReq1A);
				
				
				//Se obtiene la queue dada el rango
				IQueue<Servicio> colaReq1A = Controller.darServiciosEnRango(rangoReq1A);
				
			
				//TODO 
				//Recorra la cola y muestre cada servicio en ella
				for (int i=0; i< colaReq1A.size(); i++)
				{
					
				Servicio a =colaReq1A.dequeue();
					System.out.println( "el servicio  : "+ a.getTripId() + "  esta dentro del rango" );
					System.out.println( " la fecha de inicio del  servicio es  : "+ a.getFechaInicio()  );
				}
				
				System.out.println("el tamaņo de la cola es" + colaReq1A.size());
				break;
				
			
			case 3://Taller 4 punto 1

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialT4P1 = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialT4P1 = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalT4P1 = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalT4P1 = sc.next();
				
				
				//VORangoFechaHora
				RangoFechaHora rangoT4P1 = new RangoFechaHora(fechaInicialT4P1, fechaFinalT4P1, horaInicialT4P1, horaFinalT4P1);
				
				
				//Se obtiene la queue dada el rango
				InfoTaxiRango2[] colaT4P1 = Controller.TaxisOrdenados(rangoT4P1);
				
			
				//TODO 
				//Recorra la cola y muestre cada servicio en ella
				for (int i=0; i< colaT4P1.length; i++)
				{
					
				InfoTaxiRango2 a =colaT4P1[i];
					System.out.println( "el TAXI  : "+ a.getIdTaxi()+ "  esta dentro del rango" );
					System.out.println( " el numerp de   servicios es  : "+ a.getNumeroDeServicios() );
				}
				
				System.out.println("el tamaņo de la cola es" + colaT4P1.length);
				break;
				
				case 4://Taller 4 punto 2

					//fecha inicial
					System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
					String fechaInicialT4P2 = sc.next();

					//hora inicial
					System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
					String horaInicialT4P2 = sc.next();

					//fecha final
					System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
					String fechaFinalT4P2 = sc.next();

					//hora final
					System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
					String horaFinalT4P2 = sc.next();
					
					
					//VORangoFechaHora
					RangoFechaHora rangoT4P2 = new RangoFechaHora(fechaInicialT4P2, fechaFinalT4P2, horaInicialT4P2, horaFinalT4P2);
					
					
					//Se obtiene la queue dada el rango
					infoCompaņiaRango[] colaT4P2 = Controller.CompaņiasOrdenados(rangoT4P2);
					
				
					//TODO 
					//Recorra la cola y muestre cada servicio en ella
					for (int i=0; i< colaT4P2.length; i++)
					{
						
					infoCompaņiaRango a =colaT4P2[i];
						System.out.println( "la compaņia  : "+ a.getNombre() );
						System.out.println( " el numerp de   servicios es  : "+ a.getNumeroTotalServicios() );
						System.out.println( " el numerp de  taxis es  : "+ a.getNumeroTaxis() );
					}
					
					System.out.println("el tamaņo de la cola es" + colaT4P2.length);
					break;
					
			case 13: //salir
				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");

		System.out.println("\nParte A:\n");
		System.out.println("2.Obtenga una lista con todos los servicios de taxi ordenados cronologicamente por su fecha/hora inicial, \n"
				+ " que se prestaron en un periodo de tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta. (1A) ");
		System.out.println("3.Arreglo ordenado por identificador de taxi , taxis con servicios en periodo ");
		System.out.println("4.Arreglo ordenado por cantidad de servicios ");
		System.out.println("13. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
