package controller;


import java.text.ParseException;
import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.InfoTaxiRango2;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;
import model.vo.infoCompa�iaRango;

public class Controller 
{
	
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static TaxiTripsManager manager = new TaxiTripsManager();

	//taller 4 punto 1
		public static InfoTaxiRango2[] TaxisOrdenados(RangoFechaHora rango)
		{
			 return manager.taxisRango2Ordenados(rango);
		}
		//taller 4 punto 2
				public static infoCompa�iaRango[] Compa�iasOrdenados(RangoFechaHora rango)
				{
					 return manager.Compa�iasOrdenadas(rango);
				}
	//1C
	public static void cargarSistema(String direccionJson)
	{
		 manager.loadServices(direccionJson);
	}
	//A1
	public static IQueue<Servicio> darServiciosEnRango(RangoFechaHora rango)
	{
		return manager.darServiciosEnPeriodo(rango);
	}




}
