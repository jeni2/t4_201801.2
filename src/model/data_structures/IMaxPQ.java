package model.data_structures;

public interface IMaxPQ < Key >  {

	void insert(Key v); // inserta una clave en la cola de prioridad
	Key max() ; //devuelve la clave m�s grande
	Key delMax(); // devuelve y elimina la clave m�s grande
	boolean isEmpty();// esta la cola de prioridad vac�a?
	int size(); // n�mero de claves en la cola de prioridad
	void MaxPQ(Key[] pA, int N);//crea una cola de prioridad de capacidad m�xima inicial a partir de las claves en un []
	
	
	
}
