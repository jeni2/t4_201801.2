package model.data_structures;

public class MyMaxPQ <Key extends Comparable<Key>> implements IMaxPQ<Key> {

	private Key[] pq; // pq[i] = ith element on pq
	private int N; // number of elements on pq


	
	public MyMaxPQ(Key[] pA, int pN) {
		// TODO Auto-generated method stub
		pq=pA;
		N=pN;
	}


	@Override
	public void insert (Key x)
	{
		pq[++N] = x;
		swim(N);
	}


	@Override
	public Key max() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Key delMax() {
		// TODO Auto-generated method stub
		Key max = pq[1];
		exch(1, N--);
		sink(1);
		pq[N+1] = null;
		return max;
	}

	public Key delMaxHeap ()
	{
	Key max = pq[1];
	exch(1, N--);
	sink(1);
	return max;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		{ return N == 0; }
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return N;
	}
	public boolean less (int i, int j)
	{ 
		return pq[i].compareTo(pq[j]) < 0; 
	}
	public void exch ( int i, int j)
	{ 
		
		Key t = pq[i]; 
		pq[i] = pq[j]; 
		pq[j] = t; 
	}
	public void swim (int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k, k/2);
			k = k/2;
		}
	}
	public void sink (int k)
	{
		while (2*k <= N)
		{
			int j = 2*k;
			if (j < N && less(j, j+1)) 
				j++;
			if (!less(k, j)) 
				break;
			exch(k, j);
			k = j;
		}
	}


	
	
	public  void sortHeap(Key[] a) {
		pq=a;
		int N = a.length;
		
		for (int k = N/2; k >= 1; k--)
		sink( k);
		while (N > 1)
		{
		exch( 1, N--);
		sink( 1 );}
		}


	@Override
	public void MaxPQ(Key[] pA, int N) {
		// TODO Auto-generated method stub
		
	}


	

}

