package model.vo;

import model.data_structures.MyLinkedList;

public class infoCompa�iaRango implements Comparable<infoCompa�iaRango> {

	//Su nombre, el n�mero de taxis que aparecieron con servicios registrados en la consulta
	//inicial y el n�mero total de servicios de sus taxis en la consulta inicial
	//ATRIBUTOS
	public infoCompa�iaRango(String pNombre , int pNumeroTaxis, int pNumeroTotalServicios){
		nombre= pNombre;
		numeroTaxis= pNumeroTaxis;
		numeroTotalServicios= pNumeroTotalServicios;
	}
		/**
		 * Modela el nombre de la empresa 
		 */
		private String nombre;
		
		/**
		 * Modela el l n�mero de taxis que aparecieron con servicios registrados en la consulta
	//inicial
		 */
		private int numeroTaxis;
		
		/**
		 * modela el nombre de la compa�ia del taxi
		 */
		private int numeroTotalServicios;
		
		
	@Override
	public int compareTo(infoCompa�iaRango o) {
		// TODO Auto-generated method stub
		return 0;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getNumeroTaxis() {
		return numeroTaxis;
	}


	public void setNumeroTaxis(int numeroTaxis) {
		this.numeroTaxis = numeroTaxis;
	}


	public int getNumeroTotalServicios() {
		return numeroTotalServicios;
	}


	public void setNumeroTotalServicios(int numeroTotalServicios) {
		this.numeroTotalServicios = numeroTotalServicios;
	}

}
