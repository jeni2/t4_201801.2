package model.vo;
    
    import model.data_structures.LinkedList;
	import model.data_structures.MyLinkedList;

	/**
	 * VO utilizado para req 3A, contiene el rango en el que se pidi� generar la informaci�n del taxi y 
	 * los datos solicitados
	 */
	public class InfoTaxiRango2 implements Comparable<InfoTaxiRango2>
	{
		//ATRIBUTOS
		
		/**
		 * Modela el id del taxi 
		 */
		private String idTaxi;
		
		
		/**
		 * modela el numero total de servicios prestados
		 */
		private int numeroDeServicios;
		
		/**
		 * modela el dinero que gano el taxi en el rango
		 */
		private double plataGanada;
		
		
		/**
		 * modela la distancia recorrida por el taxi en el rango
		 */
		private double distanciaTotalRecorrida; 
		
		private String compa�ia;
		//M�TODOS
		
		/**
		 * @return the idTaxi
		 */
		public String getIdTaxi()
		{
			return idTaxi;
		}

		/**
		 * @param idTaxi the idTaxi to set
		 */
		public void setIdTaxi(String idTaxi)
		{
			this.idTaxi = idTaxi;
		}

		/**
		 * @return the rango
		 */


		public InfoTaxiRango2(String pIdTaxi,   double pPlataGanada, double pDistanciaTotalRecorrida, int pNumeroServicios, String pCompa�ia  ) {
			idTaxi= pIdTaxi;
			
			plataGanada= pPlataGanada;
			
			distanciaTotalRecorrida= pDistanciaTotalRecorrida;
			
			setCompa�ia(pCompa�ia);
			
		}
		

		/**
		 * @return the plataGanada
		 */
		public double getPlataGanada() 
		{
			return plataGanada;
		}

		/**
		 * @param plataGanada the plataGanada to set
		 */
		public void setPlataGanada(double plataGanada) 
		{
			this.plataGanada = plataGanada;
		}

		
		/**
		 * @return the distanciaTotalRecorrida
		 */
		public double getDistanciaTotalRecorrida()
		{
			return distanciaTotalRecorrida;
		}

		/**
		 * @param distanciaTotalRecorrida the distanciaTotalRecorrida to set
		 */
		public void setDistanciaTotalRecorrida(double distanciaTotalRecorrida) 
		{
			this.distanciaTotalRecorrida = distanciaTotalRecorrida;
		}

	
		

		@Override
		public int compareTo(InfoTaxiRango2 arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		public int getNumeroDeServicios() {
			return numeroDeServicios;
		}

		public void setNumeroDeServicios(int numeroDeServicios) {
			this.numeroDeServicios = numeroDeServicios;
		}

		public String getCompa�ia() {
			return compa�ia;
		}

		public void setCompa�ia(String compa�ia) {
			this.compa�ia = compa�ia;
		}
		
	}


