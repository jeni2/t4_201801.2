package model.logic;

import java.io.FileReader;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import api.ITaxiTripsManager;
import jdk.nashorn.internal.ir.CatchNode;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.MyLinkedList;
import model.data_structures.MyMaxPQ;
import model.data_structures.MyQueue;
import model.data_structures.MyStack;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.InfoTaxiRango2;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;

import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;
import model.vo.infoCompa�iaRango;
import sun.print.resources.serviceui;


public class TaxiTripsManager
{
	
	// TODO
	// Definition of data model 
	private MyLinkedList<Servicio> services = new MyLinkedList<Servicio>();
	private MyLinkedList<Taxi> taxis = new MyLinkedList<Taxi>();
	private MyLinkedList<String> ids = new MyLinkedList<String>();
	private MyLinkedList<CompaniaServicios> compa�iaServicio = new MyLinkedList<CompaniaServicios>();
	private MyLinkedList<Compania> compa�ia = new MyLinkedList<Compania>();
	private MyLinkedList<ZonaServicios> zonaServicio = new MyLinkedList<ZonaServicios>();


	public static final String[] DIRECCION_LARGE_JSON ={
		
			"./data/taxi-trips-wrvz-psew-subset-02-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-03-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-04-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-05-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-06-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-07-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-08-02-2017.json"
			
	};

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	

	DateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss" );
	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub

		services= new  MyLinkedList <Servicio>();
		taxis= new MyLinkedList<Taxi>();
		compa�ia= new MyLinkedList<Compania>();
		ids= new MyLinkedList<String>();
		compa�iaServicio = new MyLinkedList<CompaniaServicios>();
		zonaServicio = new MyLinkedList<ZonaServicios>();

		JSONParser parser =new JSONParser();
		try
		{
			MyLinkedList<Taxi>  inscritos= new MyLinkedList<Taxi>();
			MyLinkedList<Servicio> serviciosCompa= new MyLinkedList<Servicio>();
			MyLinkedList<String> idsZonas= new MyLinkedList<String>();
			MyLinkedList<Servicio> asociadosFechaZona= new MyLinkedList<Servicio>();
			MyLinkedList<FechaServicios> fechasServicios= new MyLinkedList<FechaServicios>();

			JSONArray listaGson= (JSONArray)parser.parse(new FileReader(serviceFile));
			Iterator iter=  listaGson.iterator();
			int x= 0;
			Date fechafinal = null;
			Date fechaInicio = null;

			while(iter.hasNext())
			{
				JSONObject o=(JSONObject) listaGson.get(x);

				String company= "Independents";
				if(o.get("company") != null) {
					company= (String) o.get("company");
				}
				int community =0;
				if(o.get("dropoff_community_area") != null) {
					community= Integer.parseInt((String) o.get("dropoff_community_area"));
				}

				String tripId= " ";
				if(o.get("trip_id") != null) {
					tripId= (String) o.get("trip_id");
				}

				String taxiId= " ";
				if(o.get("taxi_id") != null) {
					taxiId= (String) o.get("taxi_id");
				}

				int tripSeconds= 0;
				if(o.get("trip_seconds") != null) {
					tripSeconds= Integer.parseInt((String) o.get("trip_seconds"));
				}

				double tripMiles= 0;
				if(o.get("trip_miles") != null) {
					tripMiles= Double.parseDouble((String) o.get("trip_miles"));
				}
				double tripTotal= 0;
				if(o.get("trip_total") != null) {
					tripTotal= Double.parseDouble((String) o.get("trip_total"));
				}

				String inicio ="";
				if(o.get("trip_start_timestamp") != null)
				{
					inicio = (String) o.get("trip_start_timestamp");

					fechaInicio= formatoFecha.parse(inicio);
				}

				String fin ="";

				if(o.get("trip_end_timestamp") != null)
				{
					fin = (String) o.get("trip_end_timestamp");
					fechafinal = formatoFecha.parse(fin);

				}

				double tri= 0;
				if(o.get("trip_total") != null) {
					tripTotal= Double.parseDouble((String) o.get("trip_total"));
				}

				Taxi taxix= new Taxi(company, taxiId);
				boolean w=false;
				for(int h=0; h<ids.size() && !w; h++){
					if(ids.getI(h).equals(taxiId))
					{
						w=true;
					}
				}
				if(w==false)
				{
					taxis.add(taxix);
					ids.add(taxix.getTaxiId());
				}

				String idZonaF= null;
				if(o.get("dropoff_census_tract") != null) {
					idZonaF= (String) o.get("dropoff_census_tract");
				}
				String idZonaI=null;
				if(o.get("pickup_census_tract") != null) {
					idZonaI= (String) o.get("pickup_census_tract");
				}


				Servicio serviciox = new Servicio(taxiId, tripId, tripSeconds, tripMiles,fechaInicio, fechafinal, taxix, tripTotal, idZonaI, idZonaF );

				services.add(serviciox);

				asociadosFechaZona.add(serviciox);
				FechaServicios fechaServiciosx= new FechaServicios(inicio, asociadosFechaZona, asociadosFechaZona.size());
				fechasServicios.add(fechaServiciosx);
				ZonaServicios elemntoZOna= new ZonaServicios(idZonaI, fechasServicios);


				boolean b= false;
				for(int g=0; g<compa�ia.size()&&!b; g++){
					if(compa�ia.getI(g).getNombre().equals(company))
					{
						b=true;
						compa�ia.getI(g).getTaxisInscritos().add(taxix);
					}
				}
				if(!b)
				{
					inscritos.add(taxix);
					Compania y= new Compania(company, inscritos);
					compa�ia.add(y);
				}

				x++;

				iter.next();
				
				
			}
		

		System.out.println("La cantidad de compa�ias cargados fue: " + compa�ia.size());
		System.out.println("La cantidad de servicios cargados fue: " + services.size());
		System.out.println("La cantidad de TAXIS cargados fue: " + taxis.size());
			}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	
	public MyQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango) throws IndexOutOfBoundsException, ParseException
	{
		DateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss" );
		MyQueue<Servicio> serviciosPeriodo= new MyQueue<Servicio>();

		for (int i =0; i< services.size() ; i ++)
		{
			try {
				
					int com=services.getI(i).getFechaInicio().compareTo(formatoFecha.parse(rango.getFechaInicial()+'T'+ rango.getHoraInicio()) );
					
					int com2=services.getI(i).getFechaFinal().compareTo(formatoFecha.parse(rango.getFechaFinal()+'T'+rango.getHoraFinal()) );
					if (com>=0 && com2<=0)
					{
						serviciosPeriodo.enqueue(services.getI(i));
					}
		}
//				quicksort(lista, 0, serviciosPeriodo.size());
				
			
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return serviciosPeriodo;}

	public boolean yaEsta(String id, MyLinkedList<Taxi> a)
	{
		int tama�o = a.size();
		
		for (int i = 0 ; i < tama�o; i++)
		{
			Taxi actual = a.getI(i);
			if (actual.getTaxiId().equals(id))
			{
				return true;
			}
		}
		return false;
	}
	public boolean yaEstaCompa�ia(String nombre, infoCompa�iaRango[] a)
	{
		int tama�o = a.length;
		
		for (int i = 0 ; i < tama�o; i++)
		{
			infoCompa�iaRango actual = a[i];
			if (actual.getNombre().equals(nombre))
			{
				return true;
			}
		}
		return false;
	}
	

	public  InfoTaxiRango2[] TaxisServiciosEnPeriodo2(RangoFechaHora rango)
  {
		System.out.println("llegue2");
	MyLinkedList<Taxi> a= new MyLinkedList<Taxi>();
	IQueue<Servicio> servPeriodo= darServiciosEnPeriodo(rango); 
	int pos=0;
	InfoTaxiRango2[] b= new InfoTaxiRango2[servPeriodo.size()];
       InfoTaxiRango2 info = null;
	
	for (int i=0; i<servPeriodo.size(); i++){

		Servicio x= servPeriodo.dequeue();
		if(yaEsta(x.getTaxiId(), a)==false){
			a.add(x.getTaxiServicio());
			info= darInformacionTaxiEnRango2(x.getTaxiId(), rango);
		b[pos] = info;
		System.out.println(info.getNumeroDeServicios());
		}
		pos++;
		
	}
	return b;
}
	public  infoCompa�iaRango[] Compa�iasEnPeriodo2(RangoFechaHora rango)
	  {
		
	  
		InfoTaxiRango2[] b= TaxisServiciosEnPeriodo2(rango);
		infoCompa�iaRango[] a= new infoCompa�iaRango[b.length];
		infoCompa�iaRango info=null;
		int numeroTaxis=0;
		int totalServicios=0;
		String nombre="";
		String temp=" ";
		for(int i =0; i<b.length; i++){
			temp = b[i].getCompa�ia();
			if( nombre.equals("independiente")==false){
			for (int j=0; i<b.length;i ++){
				if(b[j].getCompa�ia().equals(temp)){
					numeroTaxis++;
					totalServicios= totalServicios+ b[j].getNumeroDeServicios();	
					nombre= b[j].getCompa�ia();
				}
				}
			
		if	(yaEstaCompa�ia(nombre, a)==false  )
		{
       info= new infoCompa�iaRango(b[i].getCompa�ia(),numeroTaxis, totalServicios);
       a[i]= info;  }
		}}
			
		return a;
	}

	public InfoTaxiRango2 darInformacionTaxiEnRango2(String id, RangoFechaHora rango)
	{
		// TODO Auto-generated method stub

		double plata=0;
		double distanciaTotal=0;
		String compa�ia= "independiente";
		MyLinkedList<Servicio> serv= new MyLinkedList<Servicio>();

		IQueue<Servicio> serviciosPeriodo= darServiciosEnPeriodo(rango);

		InfoTaxiRango2 info=null;
		for(int i=0; i<serviciosPeriodo.size(); i++ ){
			Servicio x= serviciosPeriodo.dequeue();
			if(x!=null){
				if(x.getTaxiServicio().getTaxiId().equals(id)){
					compa�ia= x.getTaxiServicio().getCompany();
					serv.add(x);
					plata= plata + x.getTrp_total();
					distanciaTotal= distanciaTotal+ x.getTripMiles();

				}

			}
		info= new InfoTaxiRango2(id, plata, distanciaTotal, serv.size(), compa�ia);}
		return info;
	}

	
	
	
	public InfoTaxiRango2[] taxisRango2Ordenados(RangoFechaHora rango){
		System.out.println("llegue 1");
		InfoTaxiRango2[] a= TaxisServiciosEnPeriodo2(rango) ;
		heapsortTaxis(a);
		 return a;
	}
	public infoCompa�iaRango[] Compa�iasOrdenadas(RangoFechaHora rango){
		System.out.println("llegue 1");
		infoCompa�iaRango[] a= Compa�iasEnPeriodo2(rango);
		heapsortCompa�ias(a);
		 return a;
	}
	
	

public	void heapsortTaxis(InfoTaxiRango2[] a)
	{
		MyMaxPQ<InfoTaxiRango2> a1= new MyMaxPQ<InfoTaxiRango2>(a, a.length);
		
		int N=a.length;
		int k;
		for(k=N/2; k>=1; k--)
			downheapTaxis(a,k);
		while(N > 1)
		{
			
			a1.exch(1,N--);
			downheapTaxis(a,1);
		}
	}
	public void downheapTaxis(InfoTaxiRango2 a[],  int r)
	{
		int N=a.length;
		int j;
		InfoTaxiRango2 v =a[r];
		 
		MyMaxPQ<InfoTaxiRango2> a1= new MyMaxPQ<InfoTaxiRango2>(a, a.length);
		
		while (r <= N/2)
		{
			j = 2*r;
			if(j < N && (a[j].getIdTaxi().compareTo(a[j+1].getIdTaxi())<0));
				j++;
			if( v.getIdTaxi().compareTo( a[j].getIdTaxi())>=0)
				break;
			a[r] = a[j];
			r = j;
		
		v= a[j];
		}
	}
public	void heapsortCompa�ias(infoCompa�iaRango[] a)
{
	MyMaxPQ<infoCompa�iaRango> a1= new MyMaxPQ<infoCompa�iaRango>(a, a.length);
	
	int N=a.length;
	int k;
	for(k=N/2; k>=1; k--)
		downheapCompa�ias(a,k);
	while(N > 1)
	{
		
		a1.exch(1,N--);
		downheapCompa�ias(a,1);
	}
}
public void downheapCompa�ias(infoCompa�iaRango a[],  int r)
{
	int N=a.length;
	int j;
	infoCompa�iaRango v =a[r];
	 
	MyMaxPQ<infoCompa�iaRango> a1= new MyMaxPQ<infoCompa�iaRango>(a, a.length);
	
	while (r <= N/2)
	{
		j = 2*r;
		if(j < N && (a[j].getNumeroTotalServicios()<(a[j+1].getNumeroTotalServicios()))){
			j++;
		if( v.getNumeroTotalServicios()>= a[j].getNumeroTotalServicios())
			break;
		a[r] = a[j];
		r = j;
	
	v= a[j];
	}
}
}}

